package com.example.tugas2papb;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.button.MaterialButton;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        EditText usernameInput =  findViewById(R.id.username);
        EditText passwordInput = findViewById(R.id.password);

        MaterialButton loginbtn = (MaterialButton) findViewById(R.id.loginbtn);

        //admin
        loginbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String username = usernameInput.getText().toString();
                String password = passwordInput.getText().toString();
                if (username.equals("215150400111038") && password.equals("admin")) {
                    //correct
                    Toast.makeText(MainActivity.this,"Jauza Aryazuhdi Alaudin",Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(MainActivity.this,MainActivity2.class);
                    intent.putExtra("username",  username);
                    intent.putExtra("password", password);
                    startActivity(intent);
                }else {
                    //incorrect
                    Toast.makeText(MainActivity.this,"Sign in Failed/nplease enter the correct username or password",Toast.LENGTH_SHORT).show();
                }

            }
        });
    }
}